#Proyecto Integrador de Cloud Computing

Pasos para la puesta en marcha de la aplicación:

1. Clonar el aplicativo para que sea descargado a su equipo, este creará una carpeta denominada
   proyectointegrador.
2. Ingresar a la carpeta proyectointegrador.
3. Ejecutar la siguiente instrucción: 
    sudo docker-compose up
4. Ejecutar un gestor de base de datos MySQL (workbench) y crear una nueva conexión con los siguientes valores:
    Host: 192.168.100.250
    usuario: root
    password: .sweetpwd.
    Puerto: 8082
5. Ejecutar el script que se encuentra dentro de la carpeta proyectointegrador/sql

Con esos pasos abrir un navegador de internet e ingresar la siguiente ruta: 192.168.100.250:8000

Se presentará la aplicación en ejecución.
